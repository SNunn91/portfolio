import Vue from 'vue';
import Vuetify from 'vuetify/lib';

Vue.use(Vuetify);

export default new Vuetify({
  theme:{
    themes: {
      light: {
        primary: '#202b35',
        secondary: '#00adff',
        tertiary: '#f5f5f5',
      },
    },
  },
  icons: {
    iconfont: 'mdi',
  },
});
